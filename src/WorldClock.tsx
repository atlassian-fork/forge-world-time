import ForgeUI, { render, useConfig, Fragment, Text, Select, Option, ConfigForm, Macro } from "@forge/ui";

import moment from 'moment-timezone';
import { formatTime, formats, getFlagForTimeZone } from './util';

const defaultConfig = {
  tz: 'America/Los_Angeles',
  format: formats[0]
};

const Config = () => {
  return (
    <ConfigForm>
      <Select label="Time Zone" name="tz">
        { moment.tz.names().map(element => {return (
          <Option label={element} value={element} />
        )})}
      </Select>
      <Select label="Format" name="format">
        { formats.map(format => {return (
          <Option 
            label={formatTime('America/Los_Angeles', format, null)} 
            value={format} />
        )})}
      </Select>      
    </ConfigForm>
  );
}

const App = () => {
  const { tz, format } = useConfig();
  const flag = getFlagForTimeZone(tz);
  const displayTime = formatTime(tz, format, null);

  return (
    <Fragment>
      <Text content={`${flag} ${displayTime}`} />
    </Fragment>
  );
};

export const run = render(
  <Macro 
    app={<App />}
    config={<Config />}
    defaultConfig={defaultConfig}
  />
);
